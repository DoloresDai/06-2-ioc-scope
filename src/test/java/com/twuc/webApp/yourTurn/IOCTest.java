package com.twuc.webApp.yourTurn;

import com.twuc.webApp.model.AbstractBaseClass;
import com.twuc.webApp.model.DerivedClass;
import com.twuc.webApp.model.ExtendInterfaceOneImpl;
import com.twuc.webApp.model.InterfaceOneImpl.InterfaceOne;
import com.twuc.webApp.model.InterfaceOneImpl.InterfaceOneImpl;
import com.twuc.webApp.model.SimplePrototypeScopeClass;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class IOCTest {
    @Test
    void should_get_the_same_instance_when_getBean_twice() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        InterfaceOne bean1 = context.getBean(InterfaceOneImpl.class);
        InterfaceOne bean2 = context.getBean(InterfaceOne.class);

        assertSame(bean1, bean2);
    }

    @Test
    void should_get_different_instance_when_getBean_of_extend_class() {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        InterfaceOneImpl bean1 = context.getBean(ExtendInterfaceOneImpl.class);
        InterfaceOneImpl bean2 = context.getBean(InterfaceOneImpl.class);

        assertNotSame(bean1, bean2);

    }

    @Test
    void should_get_the_same_instance_when_getBean_of_abstract_class() {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        AbstractBaseClass bean1 = context.getBean(DerivedClass.class);
        AbstractBaseClass bean2 = context.getBean(AbstractBaseClass.class);

        assertSame(bean1, bean2);
    }

    @Test
    void should_get_different_instance_when_use_prototype_scope() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);

        assertNotSame(bean1, bean2);
    }
}
